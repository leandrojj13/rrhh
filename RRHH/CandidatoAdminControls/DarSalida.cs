﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using RRHH.Model.Entities;
using DevExpress.XtraGrid;
using RRHH.BL.Services;

namespace RRHH.CandidatoAdminControls
{
    public partial class DarSalida : DevExpress.XtraEditors.XtraForm
    {
        public Candidato candidato { get; set; }
        public Empleado _empleado  { get; set; }
        public GridControl gridView { get; set; }
        public DarSalida(Empleado empleado, GridControl gridView, Candidato candidato)
        {
            InitializeComponent();
            this.candidato = candidato;
            this._empleado = empleado;
            this.gridView = gridView;
        }

        void windowsUIButtonPanel_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        void OnSave()
        {

            _empleado.SalarioMensual = Convert.ToDecimal(salarioMensualTxt.Text);

            var empleadoService = Program.GetIntance<IEmpleadoService>();

            var newEmpleado = new Empleado()
            {

                Cedula = _empleado.Cedula,
                Nombre = _empleado.Nombre,
                FechaIngreso = DateTime.Now,
                PuestoId = _empleado.PuestoId,
                SalarioMensual = _empleado.SalarioMensual
            };
            var resultSave = empleadoService.Save(newEmpleado);

            if (resultSave.Success)
            {
                var candidatoService = Program.GetIntance<ICandidatoService>();
                var result = candidatoService.DeleteById(this.candidato.Id);

                if (result.Success) {

                    var puestoService = Program.GetIntance<IPuestoService>();
                    var resultPuesto = puestoService.DeleteById(newEmpleado.PuestoId);

                    if (resultPuesto.Success)
                    {
                        MessageBox.Show("Se ha registrado como empleado exitosamente", "Registro de empleado", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        var listCandidato = (List<Candidato>)this.gridView.DataSource;
                        listCandidato.Remove(this.candidato);
                        this.gridView.RefreshDataSource();

                        Owner.Hide();
                        this.Hide();
                    }
                }
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Cancelar":
                    this.Hide();
                    break;
                case "Dar Entrada":
                    OnSave();
                    break;
            }

        }
    }
}