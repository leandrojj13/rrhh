﻿namespace RRHH.CandidatoAdminControls
{
    partial class ViewCandidatos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        ///
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.windowsUIButtonPanelCloseButton = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.windowsUIButtonPanelMain = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.tituloCandidato = new DevExpress.XtraEditors.LabelControl();
            this.recomendadoPorTxt = new System.Windows.Forms.TextBox();
            this.salarioTxt = new System.Windows.Forms.TextBox();
            this.departamentoTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nombreTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cedulaTxt = new System.Windows.Forms.TextBox();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControlCapacitaciones = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Nivel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FechaDesde = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FechaHasta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControlExperienciaLaboral = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Empresa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PuestoOcupado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Salario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabNavigationPage3 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControlCompetencias = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.puestoNombreTxt = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCapacitaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExperienciaLaboral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.tabNavigationPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCompetencias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // windowsUIButtonPanelCloseButton
            // 
            this.windowsUIButtonPanelCloseButton.ButtonInterval = 0;
            windowsUIButtonImageOptions1.ImageUri.Uri = "Backward;Size32x32;GrayScaled";
            this.windowsUIButtonPanelCloseButton.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("", true, windowsUIButtonImageOptions1)});
            this.windowsUIButtonPanelCloseButton.ContentAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.windowsUIButtonPanelCloseButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.windowsUIButtonPanelCloseButton.ForeColor = System.Drawing.Color.Gray;
            this.windowsUIButtonPanelCloseButton.Location = new System.Drawing.Point(0, 0);
            this.windowsUIButtonPanelCloseButton.MaximumSize = new System.Drawing.Size(45, 0);
            this.windowsUIButtonPanelCloseButton.MinimumSize = new System.Drawing.Size(45, 0);
            this.windowsUIButtonPanelCloseButton.Name = "windowsUIButtonPanelCloseButton";
            this.windowsUIButtonPanelCloseButton.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.windowsUIButtonPanelCloseButton.Padding = new System.Windows.Forms.Padding(5, 5, 0, 0);
            this.windowsUIButtonPanelCloseButton.Size = new System.Drawing.Size(45, 579);
            this.windowsUIButtonPanelCloseButton.TabIndex = 2;
            this.windowsUIButtonPanelCloseButton.Text = "windowsUIButtonPanel1";
            this.windowsUIButtonPanelCloseButton.UseButtonBackgroundImages = false;
            // 
            // windowsUIButtonPanelMain
            // 
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.FontSizeDelta = -1;
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.Options.UseBackColor = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.Options.UseFont = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.Options.UseForeColor = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Normal.FontSizeDelta = -1;
            this.windowsUIButtonPanelMain.AppearanceButton.Normal.Options.UseFont = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.FontSizeDelta = -1;
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.Options.UseBackColor = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.Options.UseFont = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.Options.UseForeColor = true;
            this.windowsUIButtonPanelMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            windowsUIButtonImageOptions2.ImageUri.Uri = "Apply;Size32x32;GrayScaled";
            windowsUIButtonImageOptions3.ImageUri.Uri = "SaveAndClose;Size32x32;GrayScaled";
            windowsUIButtonImageOptions4.ImageUri.Uri = "Cancel;Size32x32;GrayScaled";
            this.windowsUIButtonPanelMain.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Dar Entrada", true, windowsUIButtonImageOptions2),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("No aplicar", true, windowsUIButtonImageOptions3),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Cancelar", true, windowsUIButtonImageOptions4)});
            this.windowsUIButtonPanelMain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.windowsUIButtonPanelMain.EnableImageTransparency = true;
            this.windowsUIButtonPanelMain.ForeColor = System.Drawing.Color.White;
            this.windowsUIButtonPanelMain.Location = new System.Drawing.Point(0, 579);
            this.windowsUIButtonPanelMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.windowsUIButtonPanelMain.MaximumSize = new System.Drawing.Size(0, 60);
            this.windowsUIButtonPanelMain.MinimumSize = new System.Drawing.Size(60, 60);
            this.windowsUIButtonPanelMain.Name = "windowsUIButtonPanelMain";
            this.windowsUIButtonPanelMain.Size = new System.Drawing.Size(911, 60);
            this.windowsUIButtonPanelMain.TabIndex = 3;
            this.windowsUIButtonPanelMain.Text = "windowsUIButtonPanelMain";
            this.windowsUIButtonPanelMain.UseButtonBackgroundImages = false;
            this.windowsUIButtonPanelMain.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.windowsUIButtonPanelMain_Click);
            // 
            // tituloCandidato
            // 
            this.tituloCandidato.AllowHtmlString = true;
            this.tituloCandidato.Appearance.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tituloCandidato.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tituloCandidato.Appearance.Options.UseFont = true;
            this.tituloCandidato.Appearance.Options.UseForeColor = true;
            this.tituloCandidato.Appearance.Options.UseTextOptions = true;
            this.tituloCandidato.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.tituloCandidato.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.tituloCandidato.Dock = System.Windows.Forms.DockStyle.Top;
            this.tituloCandidato.Location = new System.Drawing.Point(45, 0);
            this.tituloCandidato.Name = "tituloCandidato";
            this.tituloCandidato.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.tituloCandidato.Size = new System.Drawing.Size(866, 30);
            this.tituloCandidato.TabIndex = 1;
            this.tituloCandidato.Text = "Candidato a ";
            // 
            // recomendadoPorTxt
            // 
            this.recomendadoPorTxt.BackColor = System.Drawing.SystemColors.Menu;
            this.recomendadoPorTxt.Location = new System.Drawing.Point(622, 188);
            this.recomendadoPorTxt.Name = "recomendadoPorTxt";
            this.recomendadoPorTxt.ReadOnly = true;
            this.recomendadoPorTxt.Size = new System.Drawing.Size(233, 21);
            this.recomendadoPorTxt.TabIndex = 59;
            // 
            // salarioTxt
            // 
            this.salarioTxt.BackColor = System.Drawing.SystemColors.Menu;
            this.salarioTxt.ForeColor = System.Drawing.SystemColors.WindowText;
            this.salarioTxt.Location = new System.Drawing.Point(342, 188);
            this.salarioTxt.Name = "salarioTxt";
            this.salarioTxt.ReadOnly = true;
            this.salarioTxt.Size = new System.Drawing.Size(233, 21);
            this.salarioTxt.TabIndex = 58;
            // 
            // departamentoTxt
            // 
            this.departamentoTxt.BackColor = System.Drawing.SystemColors.Menu;
            this.departamentoTxt.Location = new System.Drawing.Point(56, 188);
            this.departamentoTxt.Name = "departamentoTxt";
            this.departamentoTxt.ReadOnly = true;
            this.departamentoTxt.Size = new System.Drawing.Size(233, 21);
            this.departamentoTxt.TabIndex = 57;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(681, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 20);
            this.label5.TabIndex = 56;
            this.label5.Text = "Recomendado por";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(385, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 20);
            this.label6.TabIndex = 55;
            this.label6.Text = "Salario al que aspira";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(115, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 20);
            this.label4.TabIndex = 54;
            this.label4.Text = "Departamento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(706, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 52;
            this.label3.Text = "Puestos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(140, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 51;
            this.label2.Text = "Cédula";
            // 
            // nombreTxt
            // 
            this.nombreTxt.BackColor = System.Drawing.SystemColors.Menu;
            this.nombreTxt.Location = new System.Drawing.Point(342, 106);
            this.nombreTxt.Name = "nombreTxt";
            this.nombreTxt.ReadOnly = true;
            this.nombreTxt.Size = new System.Drawing.Size(233, 21);
            this.nombreTxt.TabIndex = 50;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(426, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 49;
            this.label1.Text = "Nombre";
            // 
            // cedulaTxt
            // 
            this.cedulaTxt.BackColor = System.Drawing.SystemColors.Menu;
            this.cedulaTxt.Location = new System.Drawing.Point(56, 106);
            this.cedulaTxt.Name = "cedulaTxt";
            this.cedulaTxt.ReadOnly = true;
            this.cedulaTxt.Size = new System.Drawing.Size(233, 21);
            this.cedulaTxt.TabIndex = 48;
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Controls.Add(this.tabNavigationPage3);
            this.tabPane1.Location = new System.Drawing.Point(56, 237);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2,
            this.tabNavigationPage3});
            this.tabPane1.RegularSize = new System.Drawing.Size(799, 277);
            this.tabPane1.SelectedPage = this.tabNavigationPage1;
            this.tabPane1.Size = new System.Drawing.Size(799, 277);
            this.tabPane1.TabIndex = 60;
            this.tabPane1.Text = "Capacitación";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.AutoScroll = true;
            this.tabNavigationPage1.Caption = "Capacitaciones";
            this.tabNavigationPage1.Controls.Add(this.gridControlCapacitaciones);
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Size = new System.Drawing.Size(781, 232);
            // 
            // gridControlCapacitaciones
            // 
            this.gridControlCapacitaciones.Location = new System.Drawing.Point(33, 26);
            this.gridControlCapacitaciones.MainView = this.gridView;
            this.gridControlCapacitaciones.Name = "gridControlCapacitaciones";
            this.gridControlCapacitaciones.Size = new System.Drawing.Size(720, 176);
            this.gridControlCapacitaciones.TabIndex = 3;
            this.gridControlCapacitaciones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Description,
            this.Nivel,
            this.FechaDesde,
            this.FechaHasta});
            this.gridView.GridControl = this.gridControlCapacitaciones;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowColumnMoving = false;
            this.gridView.OptionsCustomization.AllowGroup = false;
            this.gridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView.OptionsMenu.EnableColumnMenu = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowIndicator = false;
            // 
            // Description
            // 
            this.Description.Caption = "Descripción ";
            this.Description.FieldName = "Description";
            this.Description.Name = "Description";
            this.Description.Visible = true;
            this.Description.VisibleIndex = 0;
            // 
            // Nivel
            // 
            this.Nivel.Caption = "Nivel de Capacitación ";
            this.Nivel.FieldName = "Nivel";
            this.Nivel.Name = "Nivel";
            this.Nivel.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.Nivel.Visible = true;
            this.Nivel.VisibleIndex = 3;
            // 
            // FechaDesde
            // 
            this.FechaDesde.Caption = "Fecha Desde ";
            this.FechaDesde.FieldName = "FechaDesde";
            this.FechaDesde.Name = "FechaDesde";
            this.FechaDesde.Visible = true;
            this.FechaDesde.VisibleIndex = 1;
            // 
            // FechaHasta
            // 
            this.FechaHasta.Caption = "Fecha Hasta ";
            this.FechaHasta.FieldName = "FechaHasta";
            this.FechaHasta.Name = "FechaHasta";
            this.FechaHasta.Visible = true;
            this.FechaHasta.VisibleIndex = 2;
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Experiencia Laboral";
            this.tabNavigationPage2.Controls.Add(this.gridControlExperienciaLaboral);
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Size = new System.Drawing.Size(781, 232);
            // 
            // gridControlExperienciaLaboral
            // 
            this.gridControlExperienciaLaboral.Location = new System.Drawing.Point(33, 28);
            this.gridControlExperienciaLaboral.MainView = this.gridView1;
            this.gridControlExperienciaLaboral.Name = "gridControlExperienciaLaboral";
            this.gridControlExperienciaLaboral.Size = new System.Drawing.Size(720, 176);
            this.gridControlExperienciaLaboral.TabIndex = 7;
            this.gridControlExperienciaLaboral.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Empresa,
            this.PuestoOcupado,
            this.gridColumn3,
            this.gridColumn4,
            this.Salario});
            this.gridView1.GridControl = this.gridControlExperienciaLaboral;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // Empresa
            // 
            this.Empresa.Caption = "Empresa ";
            this.Empresa.FieldName = "Empresa";
            this.Empresa.Name = "Empresa";
            this.Empresa.Visible = true;
            this.Empresa.VisibleIndex = 0;
            // 
            // PuestoOcupado
            // 
            this.PuestoOcupado.Caption = "Puesto Ocupado";
            this.PuestoOcupado.FieldName = "PuestoOcupado";
            this.PuestoOcupado.Name = "PuestoOcupado";
            this.PuestoOcupado.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.PuestoOcupado.Visible = true;
            this.PuestoOcupado.VisibleIndex = 3;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Fecha Desde ";
            this.gridColumn3.FieldName = "FechaDesde";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Fecha Hasta ";
            this.gridColumn4.FieldName = "FechaHasta";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // Salario
            // 
            this.Salario.Caption = "Salario";
            this.Salario.FieldName = "Salario";
            this.Salario.Name = "Salario";
            this.Salario.Visible = true;
            this.Salario.VisibleIndex = 4;
            // 
            // tabNavigationPage3
            // 
            this.tabNavigationPage3.Caption = "Competencias";
            this.tabNavigationPage3.Controls.Add(this.gridControlCompetencias);
            this.tabNavigationPage3.Name = "tabNavigationPage3";
            this.tabNavigationPage3.Size = new System.Drawing.Size(781, 232);
            // 
            // gridControlCompetencias
            // 
            this.gridControlCompetencias.Location = new System.Drawing.Point(33, 24);
            this.gridControlCompetencias.MainView = this.gridView2;
            this.gridControlCompetencias.Name = "gridControlCompetencias";
            this.gridControlCompetencias.Size = new System.Drawing.Size(720, 176);
            this.gridControlCompetencias.TabIndex = 7;
            this.gridControlCompetencias.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridView2.GridControl = this.gridControlCompetencias;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsCustomization.AllowColumnMoving = false;
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView2.OptionsMenu.EnableColumnMenu = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Descripción ";
            this.gridColumn1.FieldName = "Description";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // puestoNombreTxt
            // 
            this.puestoNombreTxt.BackColor = System.Drawing.SystemColors.Menu;
            this.puestoNombreTxt.Location = new System.Drawing.Point(622, 106);
            this.puestoNombreTxt.Name = "puestoNombreTxt";
            this.puestoNombreTxt.ReadOnly = true;
            this.puestoNombreTxt.Size = new System.Drawing.Size(233, 21);
            this.puestoNombreTxt.TabIndex = 61;
            // 
            // ViewCandidatos
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(911, 639);
            this.Controls.Add(this.puestoNombreTxt);
            this.Controls.Add(this.tabPane1);
            this.Controls.Add(this.recomendadoPorTxt);
            this.Controls.Add(this.salarioTxt);
            this.Controls.Add(this.departamentoTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nombreTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cedulaTxt);
            this.Controls.Add(this.tituloCandidato);
            this.Controls.Add(this.windowsUIButtonPanelCloseButton);
            this.Controls.Add(this.windowsUIButtonPanelMain);
            this.Name = "ViewCandidatos";
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCapacitaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExperienciaLaboral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.tabNavigationPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCompetencias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanelCloseButton;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanelMain;
        private DevExpress.XtraEditors.LabelControl tituloCandidato;
        private System.Windows.Forms.TextBox recomendadoPorTxt;
        private System.Windows.Forms.TextBox salarioTxt;
        private System.Windows.Forms.TextBox departamentoTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nombreTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox cedulaTxt;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraGrid.GridControl gridControlCapacitaciones;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn Description;
        private DevExpress.XtraGrid.Columns.GridColumn Nivel;
        private DevExpress.XtraGrid.Columns.GridColumn FechaDesde;
        private DevExpress.XtraGrid.Columns.GridColumn FechaHasta;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraGrid.GridControl gridControlExperienciaLaboral;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn Empresa;
        private DevExpress.XtraGrid.Columns.GridColumn PuestoOcupado;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn Salario;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage3;
        private DevExpress.XtraGrid.GridControl gridControlCompetencias;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private System.Windows.Forms.TextBox puestoNombreTxt;
    }

}