﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using RRHH.Model.Entities;
using DevExpress.XtraGrid;
using RRHH.BL.Services;

namespace RRHH.CandidatoAdminControls
{
    public partial class ViewCandidatos : DevExpress.XtraEditors.XtraForm
    {
        public Candidato _Current { get; set; }
        public GridControl _GridControl { get; set; }
        public ViewCandidatos(Candidato Current, GridControl GridControl)
        {
            InitializeComponent();
            this._Current = Current;
            this._GridControl = GridControl;

            this.cedulaTxt.Text = _Current.Cedula;
            this.departamentoTxt.Text = _Current.Departamento;
            this.nombreTxt.Text = _Current.Nombre;
            this.puestoNombreTxt.Text = _Current.PuestoNombre;
            this.recomendadoPorTxt.Text = _Current.RecomendadoPor;
            this.salarioTxt.Text = this._Current.SalarioAspira.ToString();

            this.gridControlCapacitaciones.DataSource = _Current.Capacitaciones;
            this.gridControlCapacitaciones.RefreshDataSource();

            this.gridControlCompetencias.DataSource = _Current.Competencias;
            this.gridControlCompetencias.RefreshDataSource();

            this.gridControlExperienciaLaboral.DataSource = _Current.ExperienciasLaboral;
            this.gridControlExperienciaLaboral.RefreshDataSource();
        }

        void windowsUIButtonPanelMain_Click(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        void OnApply() {
            var newEmpleado = new Empleado()
            {
                Cedula = _Current.Cedula,
                Nombre = _Current.Nombre,
                PuestoId = _Current.PuestoId
            };

            var darSalida = new DarSalida(newEmpleado, this._GridControl, _Current);
            darSalida.ShowDialog(this);
        }
        void OnDelete() {
            var candidatoService = Program.GetIntance<ICandidatoService>();

            var dialogResult = MessageBox.Show($"¿Esta seguro que desea rechazar al candidato \"{_Current.Nombre}?\"", "Rechazar candidato", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {

                var result = candidatoService.Delete(_Current);
                if (result.Success)
                {
                    this.GetDataSource();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void GetDataSource()
        {
            this._GridControl.RefreshDataSource();
            this.Hide();
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Dar Entrada":
                    OnApply();
                    break;
                case "No Aplicar":
                    OnDelete();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }
    }
}