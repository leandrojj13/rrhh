﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using RRHH.BL.Services;
using DevExpress.XtraGrid;
using RRHH.Model.Entities;

namespace RRHH.CandidatoAdminControls
{
    public partial class CandidatoAdmin : DevExpress.XtraEditors.XtraUserControl
    {
        public Candidato Current { get; set; }

        public CandidatoAdmin()
        {
            InitializeComponent();
            Text = "Candidatos";
            this.GetDataSource();

            VisibleButton("Ver", false);

        }

        void windowsUIButtonPanel_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        #region ActionMethods
        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Imprimir":
                    gridControl.ShowRibbonPrintPreview();
                    break;
                case "Refrescar":
                    GetDataSource();
                    break;
                case "Ver":
                    OnView();
                    break;
            }

            VisibleButton("Ver", false);
        }


        public void OnView() {
            var addcompetencia = new ViewCandidatos(Current, this.gridControl);
            addcompetencia.ShowDialog(this);
        }

        public void GetDataSource()
        {
            var candidatoService = Program.GetIntance<ICandidatoService>();

            var result = candidatoService.GetAll();
            gridControl.DataSource = result.ListData;
            gridControl.Refresh();
        }
        #endregion

        private void VisibleButton(string caption, bool visible)
        {
            var a = windowsUIButtonPanel.Buttons.FirstOrDefault(x => x.Properties.Caption == caption);
            a.Properties.Visible = visible;
        }

        private void gridControl_Click(object sender, EventArgs e)
        {
            var obj = (GridControl)sender;

            var index = this.cardView1.GetSelectedRows()[0];

            Current = (Candidato)obj.FocusedView.GetRow(index);

            if (Current.Id != 0)
            {
                VisibleButton("Ver", true);
            }
        }
    }
}
