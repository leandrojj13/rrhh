﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using RRHH.CapacitacionControl;
using RRHH.Model.Entities;
using DevExpress.XtraGrid;
using RRHH.BL.Services;
using RRHH.ExperienciaLaboralControls;
using DevExpress.XtraBars.Docking2010;
using RRHH.CompetenciaUserControls;

namespace RRHH.CandidatoControls
{
    public partial class Candidato : DevExpress.XtraEditors.XtraForm
    {
        public Puesto _Puesto { get; set; }
        public RRHH.Model.Entities.Candidato _CurrentCandidato { get; set; }
        public Capacitacion Capacitacion { get; set; }
        public ExperienciaLaboral ExperienciaLaboral { get; set; }
        public Competencia Competencia { get; set; }

        public Candidato(Puesto Puesto)
        {
            InitializeComponent();
            this._Puesto = Puesto;

            var puestoService = Program.GetIntance<IPuestoService>();
            var puestoResult = puestoService.GetAll();
            comboBoxPuestos.DataSource = puestoResult.ListData;

            this.comboBoxPuestos.DisplayMember = "Nombre";
            this.comboBoxPuestos.ValueMember = "Id";

            gridControlCapacitaciones.DataSource = new List<Capacitacion>();
            gridControlExperienciaLaboral.DataSource = new List<ExperienciaLaboral>();
            gridControlCompetencias.DataSource = new List<Competencia>();

            VisibleExperieniaLaboralButton(false);
            VisibleCapacitacionButton(false);
            VisibleCompetenciaUserButton(false);
        }

        void OnSave()
        {

            var newCandidato = new RRHH.Model.Entities.Candidato
            {
                Capacitaciones = (List<Capacitacion>)this.gridControlCapacitaciones.DataSource,
                ExperienciasLaboral = (List<ExperienciaLaboral>)this.gridControlExperienciaLaboral.DataSource,
                Competencias = (List<Competencia>)this.gridControlCompetencias.DataSource,

                Cedula = cedulaTxt.Text, 
                Departamento = departamentoTxt.Text,
                Nombre = nombreTxt.Text,
                PuestoId = Convert.ToInt16(comboBoxPuestos.SelectedValue),
                SalarioAspira = Convert.ToDecimal(salarioTxt.Text),
                RecomendadoPor = recomendadoPorTxt.Text
            };

            var candidatoService = Program.GetIntance<ICandidatoService>();

            var resultSave = candidatoService.Save(newCandidato);

            if (resultSave.Success)
            {
                MessageBox.Show("Guardado Correctamente", "Registro de aplicación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Enviar":
                    OnSave();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var addCapacitacion  = new AddCapacitacion(this.gridControlCapacitaciones);
            addCapacitacion.ShowDialog(this);
            VisibleCapacitacionButton(false);
        }
        private void editarCapacitaciones_Click(object sender, EventArgs e)
        {
            var editCapacitacion = new EditCapacitacion(Capacitacion, this.gridControlCapacitaciones);
            editCapacitacion.ShowDialog(this);
            VisibleCapacitacionButton(false);
        }

        private void gridControlCapacitaciones_Click(object sender, EventArgs e)
        {
            var obj = (GridControl)sender;

            if (gridView.GetSelectedRows().Any()) {
                var index = gridView.GetSelectedRows()[0];

                Capacitacion = (Capacitacion)obj.FocusedView.GetRow(index);

                if (Capacitacion != null)
                {
                    VisibleCapacitacionButton(true);
                }
            }
          
        }
        private void VisibleCapacitacionButton( bool visible)
        {
            borrarCapacitaciones.Visible = visible;
            botonEditar.Visible = visible;
        }

        private void borrarCapacitaciones_Click(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show($"¿Esta seguro que desea eliminar la capacitación \"{Capacitacion.Description}?\"", "Eliminar capacitación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {
                var listCapacitacion =  (List<Capacitacion>)this.gridView.DataSource;
                listCapacitacion.Remove(Capacitacion);
                this.gridControlCapacitaciones.RefreshDataSource();
            }
            VisibleCapacitacionButton(false);
        }


        private void simpleButton4_Click(object sender, EventArgs e)
        {
            var addExperienciaLaboral = new AddExperienciaLaboral(this.gridControlExperienciaLaboral);
            addExperienciaLaboral.ShowDialog(this);
            VisibleExperieniaLaboralButton(false);
        }
        private void VisibleExperieniaLaboralButton(bool visible)
        {
            borrarExperiencia.Visible = visible;
            editarExperiencia.Visible = visible;
        }

        private void gridControlExperienciaLaboral_Click(object sender, EventArgs e)
        {
            var obj = (GridControl)sender;

            if (gridView1.GetSelectedRows().Any())
            {
                var index = gridView1.GetSelectedRows()[0];

                ExperienciaLaboral = (ExperienciaLaboral)obj.FocusedView.GetRow(index);

                if (ExperienciaLaboral != null)
                {
                    VisibleExperieniaLaboralButton(true);
                }
            }
        }

        private void borrarExperiencia_Click(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show($"¿Esta seguro que desea eliminar esta experiencia laboral en la empresa \"{ExperienciaLaboral.Empresa}?\"", "Eliminar experiencia laboral", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {
                var listExperienciaLaboral = (List<ExperienciaLaboral>)this.gridView1.DataSource;
                listExperienciaLaboral.Remove(ExperienciaLaboral);
                this.gridControlExperienciaLaboral.RefreshDataSource();
            }
            VisibleExperieniaLaboralButton(false);
        }

        private void editarExperiencia_Click(object sender, EventArgs e)
        {
            var editExperienciaLaboral = new EditExperienciaLaboral(ExperienciaLaboral, this.gridControlExperienciaLaboral);
            editExperienciaLaboral.ShowDialog(this);
            VisibleExperieniaLaboralButton(false);
        }
        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }


        private void simpleButton5_Click(object sender, EventArgs e)
        {
            var addCompetenciaUser = new AddCompetenciaUser(this.gridControlCompetencias);
            addCompetenciaUser.ShowDialog(this);
            VisibleCompetenciaUserButton(false);
        }

        private void VisibleCompetenciaUserButton(bool visible)
        {
            botonBorrarCompetencia.Visible = visible;
        }

        private void botonBorrarCompetencia_Click(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show($"¿Esta seguro que desea eliminar competencia \"{Competencia.Description}?\"", "Eliminar competencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {
                var listCompetencia = (List<Competencia>)this.gridView2.DataSource;
                listCompetencia.Remove(Competencia);
                this.gridControlCompetencias.RefreshDataSource();
            }
            VisibleCompetenciaUserButton(false);
        }

        private void gridControlCompetencias_Click(object sender, EventArgs e)
        {
            var obj = (GridControl)sender;

            if (gridView2.GetSelectedRows().Any())
            {
                var index = gridView2.GetSelectedRows()[0];

                Competencia = (Competencia)obj.FocusedView.GetRow(index);

                if (Competencia != null)
                {
                    VisibleCompetenciaUserButton(true);
                }
            }
        }
    }
}