﻿using RRHH.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH
{
    public static class CurrentInfo
    {
        public static User CurrentUser { get; set; }
    }
}
