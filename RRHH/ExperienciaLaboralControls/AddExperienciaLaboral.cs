﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraBars.Docking2010;
using RRHH.Model.Entities;

namespace RRHH.ExperienciaLaboralControls
{
    public partial class AddExperienciaLaboral : DevExpress.XtraEditors.XtraForm
    {

        private GridControl gridView;


        public AddExperienciaLaboral(GridControl gridView)
        {
            InitializeComponent();
            this.gridView = gridView;
        }
        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Guardar":
                    OnSave();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnSave()
        {

            var newExperienciaLaboral = new ExperienciaLaboral
            {
                Empresa = empresaTxt.Text,
                FechaDesde = dateTimePickerFechaDesde.Value,
                FechaHasta = dateTimePickerFechaHasta.Value,
                PuestoOcupado = puestoOcupadoTxt.Text,
                Salario = Convert.ToDecimal(salarioTxt.Text)
            };
            var ds = (List<ExperienciaLaboral>)this.gridView.DataSource;

            if (ds == null)
            {
                ds = new List<ExperienciaLaboral>();
            }

            ds.Add(newExperienciaLaboral);
            this.gridView.RefreshDataSource();
            this.Hide();
        }


        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }
    }
}