﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using RRHH.Model.Entities;
using DevExpress.XtraGrid;
using DevExpress.XtraBars.Docking2010;

namespace RRHH.ExperienciaLaboralControls
{
    public partial class EditExperienciaLaboral : DevExpress.XtraEditors.XtraForm
    {

        public GridControl _grid { get; set; }
        public ExperienciaLaboral _Current { get; set; }
        public EditExperienciaLaboral(ExperienciaLaboral Current, GridControl grid)
        {
            InitializeComponent();
            this._grid = grid;
            this._Current = Current;

            this.empresaTxt.Text = this._Current.Empresa;
            this.dateTimePickerFechaDesde.Value = _Current.FechaDesde;
            this.dateTimePickerFechaHasta.Value = _Current.FechaHasta;
            this.puestoOcupadoTxt.Text = _Current.PuestoOcupado;
            this.salarioTxt.Text = _Current.Salario.ToString();
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Actualizar":
                    OnActualizar();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnActualizar()
        {
            this._Current.Empresa = empresaTxt.Text;
            this._Current.FechaDesde = this.dateTimePickerFechaDesde.Value;
            this._Current.FechaHasta = this.dateTimePickerFechaHasta.Value;
            this._Current.Salario = Convert.ToDecimal(this.salarioTxt.Text);
            this._Current.PuestoOcupado = this.puestoOcupadoTxt.Text;
            this._grid.RefreshDataSource();
            this.Hide();
        }

    }
}