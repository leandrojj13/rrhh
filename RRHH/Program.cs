﻿using RRHH.BL.DI;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RRHH
{
    static class Program
    {
        public static  Container container;

        public static void Bootstrap() {
            container = Registry.GetContainer();
            container.Verify();
        }

        public static T GetIntance<T> ()  
        {
            return (T)container.GetInstance(typeof(T));
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Bootstrap();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(container.GetInstance<Layout>());
        }
    }
}
