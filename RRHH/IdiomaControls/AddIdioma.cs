﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using RRHH.Model.Entities;
using RRHH.Model.Context;
using DevExpress.XtraGrid.Views.Grid;
using RRHH.BL.Services;
using DevExpress.XtraBars.Docking2010;

namespace RRHH
{
    public partial class AddIdioma : DevExpress.XtraEditors.XtraForm
    {
        private GridView gridView;

        public AddIdioma(GridView gridView)
        {
            InitializeComponent();
            this.gridView = gridView;
        }
        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Guardar":
                    OnSave();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnSave() {

            var newIdioma = new Idioma
            {
                Nombre = nombreTxt.Text
            };
            var idiomaService = Program.GetIntance<IIdiomaService>();

            var resultSave = idiomaService.Save(newIdioma);

            if (resultSave.Success)
            {
                MessageBox.Show("Guardado Correctamente", "Registro de idioma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                var ds = (List<Idioma>)this.gridView.DataSource;
                ds.Add(newIdioma);
                this.gridView.RefreshData();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }
    }
}