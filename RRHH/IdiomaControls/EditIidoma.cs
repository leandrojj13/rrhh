﻿using DevExpress.XtraBars.Docking2010;
using RRHH.BL.Services;
using RRHH.Model.Context;
using RRHH.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RRHH
{
    public partial class EditIdioma : Form
    {
        public Idioma _Current  { get; set; }
        public EditIdioma(Idioma Current)
        {
            InitializeComponent();
            this._Current = Current;

           this.nombreTxt.Text =  this._Current.Nombre;
        }


        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Actualizar":
                    OnActualizar();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnActualizar()
        {
            this._Current.Nombre = nombreTxt.Text;

            var idiomaService = Program.GetIntance<IIdiomaService>();
            var resultSave = idiomaService.Update(this._Current);

            if (resultSave.Success)
            {
                MessageBox.Show("Editado Correctamente", "Registro de idioma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }
    }
}
