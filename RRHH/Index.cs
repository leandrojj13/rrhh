﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using RRHH.BL.Services;
using DevExpress.XtraGrid;
using RRHH.Model.Entities;

namespace RRHH
{
    public partial class Index : DevExpress.XtraEditors.XtraUserControl
    {
        public Puesto Current { get; set; }
        public Index()
        {
            InitializeComponent();

            this.Text = "Inicio";
            GetDataSource();
        }

        public void GetDataSource()
        {
            var puestoService = Program.GetIntance<IPuestoService>();

            var result = puestoService.GetAll();
            gridControl.DataSource = result.ListData;
            gridControl.Refresh();
        }

        void windowsUIButtonPanel_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Imprimir":
                    gridControl.ShowRibbonPrintPreview();
                    break;
                case "Aplicar":
                    OnApply();
                    break;
            }

            VisibleButton("Aplicar", false);
        }

        void OnApply()
        {
            var candidato = new RRHH.CandidatoControls.Candidato(Current);
            candidato.ShowDialog(this);
        }

        private void gridControl_Click(object sender, EventArgs e)
        {
            var obj = (GridControl)sender;

            var index = cardView1.GetSelectedRows()[0];

            Current = (Puesto)obj.FocusedView.GetRow(index);

        }

        private void VisibleButton(string caption, bool visible)
        {
            var a = windowsUIButtonPanel.Buttons.FirstOrDefault(x => x.Properties.Caption == caption);
            a.Properties.Visible = visible;
        }
    }
}
