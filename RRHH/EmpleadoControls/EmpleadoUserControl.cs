﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using RRHH.BL.Services;

namespace RRHH.EmpleadoControls
{
    public partial class EmpleadoUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        public EmpleadoUserControl()
        {
            InitializeComponent();
            Text = "Empleados";
            GetDataSource();
        }

        public void GetDataSource()
        {
            var empleadoService = Program.GetIntance<IEmpleadoService>();

            var result = empleadoService.GetAll();
            gridControl.DataSource = result.ListData;
            gridControl.Refresh();
        }

        void windowsUIButtonPanel_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Imprimir":
                    gridControl.ShowRibbonPrintPreview();
                    break;
                case "Refrescar":
                    GetDataSource();
                    break;
            }
        }

    }
}
