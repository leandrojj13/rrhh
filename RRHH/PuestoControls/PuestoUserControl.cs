﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using RRHH.Model.Entities;
using RRHH.BL.Services;
using DevExpress.XtraGrid;

namespace RRHH.PuestoControls
{
    public partial class PuestoUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        public Puesto Current { get; set; }

        public PuestoUserControl()
        {
            InitializeComponent();
            this.Text = "Puestos";

            VisibleButton("Editar", false);
            VisibleButton("Borrar", false);
            GetDataSource();
        }
        void windowsUIButtonPanel_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        #region ActionMethods
        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Imprimir":
                    gridControl.ShowRibbonPrintPreview();
                    break;
                case "Nuevo":
                    OnNew();
                    break;
                case "Refrescar":
                    GetDataSource();
                    break;
                case "Editar":
                    OnEdit();
                    break;
                case "Borrar":
                    OnDelete();
                    break;
            }

            VisibleButton("Editar", false);
            VisibleButton("Borrar", false);
        }
        public void OnNew()
        {
            var addPuesto = new AddPuesto(this.gridView);
            addPuesto.ShowDialog(this);
        }
        public void OnEdit()
        {
            var editPuesto = new EditPuesto(Current);
            editPuesto.ShowDialog(this);
        }
        public void OnDelete()
        {

            var PuestoService = Program.GetIntance<IPuestoService>();

            var dialogResult = MessageBox.Show($"¿Esta seguro que desea eliminar el Puesto \"{Current.Nombre}?\"", "Eliminar Puesto", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {

                var result = PuestoService.Delete(Current);
                if (result.Success)
                {
                    this.GetDataSource();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public void GetDataSource()
        {
            var PuestoService = Program.GetIntance<IPuestoService>();

            var result = PuestoService.GetAll();
            gridControl.DataSource = result.ListData;
            gridControl.Refresh();
        }
        #endregion

        #region Events
        private void gridControl_Click(object sender, EventArgs e)
        {
            var obj = (GridControl)sender;

            var index = gridView.GetSelectedRows()[0];

            Current = (Puesto)obj.FocusedView.GetRow(index);

            if (Current.Id != 0)
            {
                VisibleButton("Editar", true);
                VisibleButton("Borrar", true);
            }
        }

        #endregion
        private void VisibleButton(string caption, bool visible)
        {
            var a = windowsUIButtonPanel.Buttons.FirstOrDefault(x => x.Properties.Caption == caption);
            a.Properties.Visible = visible;
        }

    }
}
