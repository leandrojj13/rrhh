﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using RRHH.BL.Services;
using RRHH.Model.Entities;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraBars.Docking2010;
using RRHH.Model.Enums;

namespace RRHH.PuestoControls
{
    public partial class AddPuesto : DevExpress.XtraEditors.XtraForm
    {
        private GridView gridView;

        public AddPuesto(GridView gridView)
        {
            InitializeComponent();
            this.gridView = gridView;

            var enumService = Program.GetIntance<IEnumService>();
            var enumNivelCapacitacionesResult = enumService.GetAllNivelRiesgoPuesto();
            comboBoxNivelRiesgo.DataSource = enumNivelCapacitacionesResult.ListData;

            this.comboBoxNivelRiesgo.DisplayMember = "Key";
            this.comboBoxNivelRiesgo.ValueMember = "Value";
        }
        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Guardar":
                    OnSave();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnSave()
        {

            var newPuesto = new Puesto
            {
                Nombre = nombreTxt.Text,
                NivelRiesgo = (NivelRiesgoPuesto)comboBoxNivelRiesgo.SelectedValue,
                MaximoSalario = Convert.ToDecimal(salarioMaximoTxt.Text),
                MinimoSalario = Convert.ToDecimal(salarioMinimoTxt.Text)
            };
            var PuestoService = Program.GetIntance<IPuestoService>();

            var resultSave = PuestoService.Save(newPuesto);

            if (resultSave.Success)
            {
                MessageBox.Show("Guardado Correctamente", "Registro de Puesto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                var ds = (List<Puesto>)this.gridView.DataSource;
                ds.Add(newPuesto);
                this.gridView.RefreshData();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }
    }
}