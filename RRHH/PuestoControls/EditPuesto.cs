﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using RRHH.BL.Services;
using RRHH.Model.Entities;
using DevExpress.XtraBars.Docking2010;
using RRHH.Model.Enums;

namespace RRHH.PuestoControls
{
    public partial class EditPuesto : DevExpress.XtraEditors.XtraForm
    {

        public Puesto _Current { get; set; }
        public EditPuesto(Puesto Current)
        {
            InitializeComponent();
            this._Current = Current;

            var enumService = Program.GetIntance<IEnumService>();
            var enumNivelCapacitacionesResult = enumService.GetAllNivelRiesgoPuesto();
            comboBoxNivelRiesgo.DataSource = enumNivelCapacitacionesResult.ListData;

            this.comboBoxNivelRiesgo.DisplayMember = "Key";
            this.comboBoxNivelRiesgo.ValueMember = "Value";

            this.nombreTxt.Text = this._Current.Nombre;
            this.comboBoxNivelRiesgo.SelectedItem = this._Current.NivelRiesgo;
            this.salarioMaximoTxt.Text = this._Current.MaximoSalario.ToString();
            this.salarioMinimoTxt.Text = this._Current.MinimoSalario.ToString();
        }


        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Actualizar":
                    OnActualizar();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnActualizar()
        {
            this._Current.Nombre = nombreTxt.Text;
            this._Current.NivelRiesgo = (NivelRiesgoPuesto)comboBoxNivelRiesgo.SelectedValue;
            this._Current.MaximoSalario = Convert.ToDecimal(salarioMaximoTxt.Text);
            this._Current.MinimoSalario = Convert.ToDecimal(salarioMinimoTxt.Text);


            var PuestoService = Program.GetIntance<IPuestoService>();
            var resultSave = PuestoService.Update(this._Current);

            if (resultSave.Success)
            {
                MessageBox.Show("Editado Correctamente", "Registro de Puesto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

    }
}