﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Docking2010.Views;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Navigation;
using RRHH.CompetenciaControls;
using RRHH.CapacitacionControl;
using RRHH.PuestoControls;
using RRHH.CandidatoAdminControls;
using RRHH.EmpleadoControls;
using RRHH.CandidatoControls;

namespace RRHH
{
    public partial class Layout : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public List<AccordionControlElement> ListAcordion { get; set; }
        public List<BarButtonItem> ListBarButtons { get; set; }

        public Layout()
        {
            InitializeComponent();
            init();
         
        }

        void init() {

            accordionControl.SelectedElement = accordionControlInicio;

            ListBarButtons = new List<BarButtonItem>() {
                barButtonItem1,  barButtonItem2,
                barButtonItem3,  barButtonItem4,
                barButtonItem5,  barButtonItem6,
                barButtonItem7,  barButtonLogOut,
                buttonIniciarSesion
            };

            ListAcordion = new List<AccordionControlElement>() {
                accordionControlIdioma,  accordionControlCompetencia,
                accordionControlPuesto,  accordionControlCandidatos,
                accordionControlInicio,  accordionControlEmpleados,
            };


            HideElementWhenIsNoLogged();
        }

        XtraUserControl ManageTabView(string caption) {
            XtraUserControl xtraUserControl = null;
            
            switch (caption) {
                case "Idiomas":
                    xtraUserControl = Program.GetIntance<IdiomasUserControl>();
                    break;
                case "Competencias":
                    xtraUserControl = Program.GetIntance<CompetenciaUserControl>();
                    break;
                case "Capacitaciones":
                    xtraUserControl = Program.GetIntance<CapacitacionUserControl>();
                    break;
                case "Puestos":
                    xtraUserControl = Program.GetIntance<PuestoUserControl>();
                    break;
                case "Inicio":
                    xtraUserControl = Program.GetIntance<Index>();
                    break;
                case "Candidatos":
                    xtraUserControl = Program.GetIntance<CandidatoAdmin>();
                    break;
                case "Empleados":
                    xtraUserControl = Program.GetIntance<EmpleadoUserControl>();
                    break;
                default:
                    xtraUserControl = Program.GetIntance<EmpleadoUserControl>();
                    break;
            }
            if (xtraUserControl != null) {
                tabbedView.AddDocument(xtraUserControl);
                tabbedView.ActivateDocument(xtraUserControl);

            }

            return xtraUserControl;
        }

        AccordionControlElement ManageAccordionView(string caption)
        {
            switch (caption)
            {
                case "Idiomas":
                    return accordionControlIdioma;
                case "Competencias":
                    return accordionControlCompetencia;
                case "Puestos":
                    return accordionControlPuesto;
                case "Candidatos":
                    return accordionControlCandidatos;
                case "Empleados":
                    return accordionControlEmpleados;
                case "Inicio":
                    return accordionControlInicio;

            }
            return accordionControlIdioma;
        }

        void accordionControl_SelectedElementChanged(object sender, SelectedElementChangedEventArgs e)
        {
            if (e.Element == null) return;

            ManageTabView(e.Element.Text);
        }
        void barButtonNavigation_ItemClick(object sender, ItemClickEventArgs e)
        {
            int barItemIndex = barSubItemNavigation.ItemLinks.IndexOf(e.Link);
            accordionControl.SelectedElement = mainAccordionGroup.Elements[barItemIndex];
        }
        void tabbedView_DocumentClosed(object sender, DocumentEventArgs e)
        {
            SetAccordionSelectedElement(e);
        }
        void SetAccordionSelectedElement(DocumentEventArgs e)
        {
            if (tabbedView.Documents.Count != 0)
            {
                accordionControl.SelectedElement = ManageAccordionView(e.Document.Caption);
            }
            else
            {
                accordionControl.SelectedElement = null;
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraUserControl xtraUserControl = Program.GetIntance<IdiomasUserControl>();

            tabbedView.AddDocument(xtraUserControl);
            tabbedView.ActivateDocument(xtraUserControl);
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraUserControl xtraUserControl = Program.GetIntance<CompetenciaUserControl>();

            tabbedView.AddDocument(xtraUserControl);
            tabbedView.ActivateDocument(xtraUserControl);

        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraUserControl xtraUserControl = Program.GetIntance<PuestoUserControl>();

            tabbedView.AddDocument(xtraUserControl);
            tabbedView.ActivateDocument(xtraUserControl);
        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraUserControl xtraUserControl = Program.GetIntance<CandidatoAdmin>();
            tabbedView.AddDocument(xtraUserControl);
            tabbedView.ActivateDocument(xtraUserControl);
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraUserControl xtraUserControl = Program.GetIntance<EmpleadoUserControl>();
            tabbedView.AddDocument(xtraUserControl);
            tabbedView.ActivateDocument(xtraUserControl);
        }

        private void barButtonItem7_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraUserControl xtraUserControl = Program.GetIntance<Index>();
            tabbedView.AddDocument(xtraUserControl);
            tabbedView.ActivateDocument(xtraUserControl);
        }

        private void buttonIniciarSesion_ItemClick(object sender, ItemClickEventArgs e)
        {
            var login = new Login(ListBarButtons, ListAcordion);
            login.ShowDialog(this);
        }

        void HideElementWhenIsNoLogged()
        {

            foreach (var item in ListAcordion)
            {
                item.Visible = false;

                if (item.Text == "Inicio")
                {
                    item.Visible = true;
                }
            }

            foreach (var item in ListBarButtons)
            {
                item.Visibility = BarItemVisibility.Never;

                if (item.Caption == "Inicio" || item.Caption.StartsWith("Iniciar Sesi"))
                {
                    item.Visibility = BarItemVisibility.Always;
                }
            }
        }
        private void barButtonLogOut_ItemClick(object sender, ItemClickEventArgs e)
        {
            CurrentInfo.CurrentUser = null;
            HideElementWhenIsNoLogged();
        }
    }
}