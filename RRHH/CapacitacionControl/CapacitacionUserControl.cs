﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using RRHH.BL.Services;
using RRHH.Model.Entities;

namespace RRHH.CapacitacionControl
{
    public partial class CapacitacionUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        public Capacitacion Current { get; set; }

        public CapacitacionUserControl()
        {
            InitializeComponent();
            Text = "Capacitaciones";

            VisibleButton("Editar", false);
            VisibleButton("Borrar", false);
            GetDataSource();

        }


        void windowsUIButtonPanel_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }
        private void VisibleButton(string caption, bool visible)
        {
            var a = windowsUIButtonPanel.Buttons.FirstOrDefault(x => x.Properties.Caption == caption);
            a.Properties.Visible = visible;
        }
        #region ActionMethods
        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Imprimir":
                    gridControl.ShowRibbonPrintPreview();
                    break;
                case "Nuevo":
                    OnNew();
                    break;
                case "Refrescar":
                    GetDataSource();
                    break;
                case "Editar":
                    OnEdit();
                    break;
                case "Borrar":
                    OnDelete();
                    break;
            }

            VisibleButton("Editar", false);
            VisibleButton("Borrar", false);
        }
        public void OnNew()
        {
            var addCapacitacion = new AddCapacitacion(this.gridControl);
            addCapacitacion.ShowDialog(this);
        }
        public void OnEdit()
        {
            //var editCapacitacion = new EditCapacitacion(Current);
            //editCapacitacion.ShowDialog(this);
        }
        public void OnDelete()
        {

            var CapacitacionService = Program.GetIntance<ICapacitacionService>();

            var dialogResult = MessageBox.Show($"¿Esta seguro que desea eliminar el Capacitacion \"{Current.Description}?\"", "Eliminar Capacitacion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {

                var result = CapacitacionService.Delete(Current);
                if (result.Success)
                {
                    this.GetDataSource();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public void GetDataSource()
        {
            var CapacitacionService = Program.GetIntance<ICapacitacionService>();

            var result = CapacitacionService.GetAll();
            gridControl.DataSource = result.ListData;
            gridControl.Refresh();
        }
        #endregion
    }
}
