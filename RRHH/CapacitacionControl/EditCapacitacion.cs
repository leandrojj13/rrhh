﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using DevExpress.XtraBars.Docking2010;
using RRHH.Model.Entities;
using RRHH.BL.Services;
using RRHH.Model.Enums;
using DevExpress.XtraGrid;

namespace RRHH.CapacitacionControl
{
    public partial class EditCapacitacion : DevExpress.XtraEditors.XtraForm
    {

        public GridControl _grid { get; set; }
        public Capacitacion _Current { get; set; }

        public EditCapacitacion(Capacitacion Current, GridControl grid)
        {
            InitializeComponent();
            this._Current = Current;
            this._grid = grid;
            var enumService = Program.GetIntance<IEnumService>();
            var enumNivelCapacitacionesResult = enumService.GetAllNivelCapacitacion();
            comboBoxNivelCapacitacion.DataSource = enumNivelCapacitacionesResult.ListData;

            this.comboBoxNivelCapacitacion.DisplayMember = "Key";
            this.comboBoxNivelCapacitacion.ValueMember = "Value";

            this.descripcionTxt.Text = this._Current.Description;
            this.dateTimePickerFechaDesde.Value = _Current.FechaDesde;
            this.dateTimePickerFechaHasta.Value = _Current.FechaHasta;
            this.comboBoxNivelCapacitacion.SelectedItem = _Current.Nivel;
        }


        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Actualizar":
                    OnActualizar();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnActualizar()
        {
            this._Current.Description = descripcionTxt.Text;
            this._Current.FechaDesde = this.dateTimePickerFechaDesde.Value;
            this._Current.FechaHasta = this.dateTimePickerFechaHasta.Value;
            this._Current.Nivel = (NivelCapacitacion)comboBoxNivelCapacitacion.SelectedValue;

            this._grid.RefreshDataSource();
            this.Hide();
        }

    }
}