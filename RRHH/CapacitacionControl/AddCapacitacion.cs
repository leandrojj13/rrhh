﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using DevExpress.XtraGrid.Views.Grid;
using RRHH.Model.Entities;
using RRHH.BL.Services;
using RRHH.Model.Enums;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraGrid;

namespace RRHH.CapacitacionControl
{
    public partial class AddCapacitacion : DevExpress.XtraEditors.XtraForm
    {
        private GridControl gridView;
        public AddCapacitacion(GridControl gridView)
        {
            InitializeComponent();
            this.gridView = gridView;


            var enumService = Program.GetIntance<IEnumService>();
            var enumNivelCapacitacionesResult = enumService.GetAllNivelCapacitacion();
            comboBoxNivelCapacitacion.DataSource = enumNivelCapacitacionesResult.ListData;

            this.comboBoxNivelCapacitacion.DisplayMember = "Key";
            this.comboBoxNivelCapacitacion.ValueMember = "Value";
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Guardar":
                    OnSave();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnSave()
        {

            var newCapacitacion = new Capacitacion
            {
                Description = descripcionTxt.Text,
                Nivel = (NivelCapacitacion)comboBoxNivelCapacitacion.SelectedValue,
                FechaDesde = dateTimePickerFechaDesde.Value,
                FechaHasta = dateTimePickerFechaHasta.Value
            };
            var ds = (List<Capacitacion>)this.gridView.DataSource;

            if (ds == null) {
                ds = new List<Capacitacion>();
            }

            ds.Add(newCapacitacion);
            this.gridView.RefreshDataSource();
            this.Hide();
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }
    }
}