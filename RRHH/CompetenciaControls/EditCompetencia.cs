﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using RRHH.Model.Entities;
using RRHH.BL.Services;
using DevExpress.XtraBars.Docking2010;

namespace RRHH.CompetenciaControls
{
    public partial class EditCompetencia : DevExpress.XtraEditors.XtraForm
    {
        public Competencia _Current { get; set; }

        public EditCompetencia(Competencia Current)
        {
            InitializeComponent();
            this._Current = Current;

            this.descripcionTxt.Text = this._Current.Description;
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Actualizar":
                    OnActualizar();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnActualizar()
        {
            this._Current.Description = descripcionTxt.Text;

            var CompetenciaService = Program.GetIntance<ICompetenciaService>();
            var resultSave = CompetenciaService.Update(this._Current);

            if (resultSave.Success)
            {
                MessageBox.Show("Editado Correctamente", "Registro de Competencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

    }
}