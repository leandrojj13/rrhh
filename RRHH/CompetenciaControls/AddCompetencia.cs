﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using DevExpress.XtraGrid.Views.Grid;
using RRHH.BL.Services;
using RRHH.Model.Entities;
using DevExpress.XtraBars.Docking2010;

namespace RRHH.CompetenciaControls
{
    public partial class AddCompetencia : DevExpress.XtraEditors.XtraForm
    {
        private GridView gridView;

        public AddCompetencia(GridView gridView)
        {
            InitializeComponent();
            this.gridView = gridView;

        }
        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Guardar":
                    OnSave();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnSave()
        {

            var newCompetencia = new Competencia
            {
                Description = descripcionTxt.Text
            };
            var CompetenciaService = Program.GetIntance<ICompetenciaService>();

            var resultSave = CompetenciaService.Save(newCompetencia);

            if (resultSave.Success)
            {
                MessageBox.Show("Guardado Correctamente", "Registro de Competencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                var ds = (List<Competencia>)this.gridView.DataSource;
                ds.Add(newCompetencia);
                this.gridView.RefreshData();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }
    }
}