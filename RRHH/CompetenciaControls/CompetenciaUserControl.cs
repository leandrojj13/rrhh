﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RRHH.Model.Entities;
using RRHH.BL.Services;
using DevExpress.XtraGrid;

namespace RRHH.CompetenciaControls
{
    public partial class CompetenciaUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        public Competencia Current { get; set; }


        public CompetenciaUserControl()
        {
            InitializeComponent();
            this.Text = "Competencias";

            VisibleButton("Editar", false);
            VisibleButton("Borrar", false);
            GetDataSource();
        }

        void windowsUIButtonPanel_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        #region ActionMethods
        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Imprimir":
                    gridControl.ShowRibbonPrintPreview();
                    break;
                case "Nuevo":
                    OnNew();
                    break;
                case "Refrescar":
                    GetDataSource();
                    break;
                case "Editar":
                    OnEdit();
                    break;
                case "Borrar":
                    OnDelete();
                    break;
            }

            VisibleButton("Editar", false);
            VisibleButton("Borrar", false);
        }
        public void OnNew()
        {
            var addcompetencia = new AddCompetencia(this.gridView);
            addcompetencia.ShowDialog(this);
        }
        public void OnEdit()
        {
            var editcompetencia = new EditCompetencia(Current);
            editcompetencia.ShowDialog(this);
        }
        public void OnDelete()
        {

            var competenciaService = Program.GetIntance<ICompetenciaService>();

            var dialogResult = MessageBox.Show($"¿Esta seguro que desea eliminar el competencia \"{Current.Description}?\"", "Eliminar competencia", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {

                var result = competenciaService.Delete(Current);
                if (result.Success)
                {
                    this.GetDataSource();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public void GetDataSource()
        {
            var competenciaService = Program.GetIntance<ICompetenciaService>();

            var result = competenciaService.GetAll();
            gridControl.DataSource = result.ListData;
            gridControl.Refresh();
        }
        #endregion

        private void VisibleButton(string caption, bool visible)
        {
            var a = windowsUIButtonPanel.Buttons.FirstOrDefault(x => x.Properties.Caption == caption);
            a.Properties.Visible = visible;
        }

        #region Events
        private void gridControl_Click(object sender, EventArgs e)
        {
            var obj = (GridControl)sender;

            var index = gridView.GetSelectedRows()[0];

            Current = (Competencia)obj.FocusedView.GetRow(index);

            if (Current.Id != 0)
            {
                VisibleButton("Editar", true);
                VisibleButton("Borrar", true);
            }
        }

        #endregion
    }
}
