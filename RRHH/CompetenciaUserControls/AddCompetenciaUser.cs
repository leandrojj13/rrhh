﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using DevExpress.XtraGrid;
using RRHH.Model.Entities;
using DevExpress.XtraBars.Docking2010;
using RRHH.BL.Services;

namespace RRHH.CompetenciaUserControls
{
    public partial class AddCompetenciaUser : DevExpress.XtraEditors.XtraForm
    {

        private GridControl GridView { get; set; }

        public AddCompetenciaUser(GridControl GridView)
        {
            InitializeComponent();
            this.GridView = GridView;

            var competenciaService = Program.GetIntance<ICompetenciaService>();
            var competenciasResult = competenciaService.GetAll();
            comboBoxCompetencia.DataSource = competenciasResult.ListData;

            this.comboBoxCompetencia.DisplayMember = "Description";
            this.comboBoxCompetencia.ValueMember = "Id";
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Guardar":
                    OnSave();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void OnSave()
        {

            var competencia = (Competencia) this.comboBoxCompetencia.SelectedItem;

            var ds = (List<Competencia>)this.GridView.DataSource;

            if (ds == null)
            {
                ds = new List<Competencia>();
            }

            ds.Add(competencia);
            this.GridView.RefreshDataSource();
            this.Hide();
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }
    }
}