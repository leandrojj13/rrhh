﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using DevExpress.XtraGrid;
using RRHH.Model.Entities;
using RRHH.BL.Services;
using DevExpress.XtraBars.Docking2010;

namespace RRHH.CompetenciaUserControls
{
    public partial class EditCompetenciaUser : DevExpress.XtraEditors.XtraForm
    {

        public GridControl _grid { get; set; }
        public Competencia _Current { get; set; }

        public EditCompetenciaUser(Competencia Current, GridControl grid)
        {
            InitializeComponent();
            this._grid = grid;
            this._Current = Current;

            var competenciaService = Program.GetIntance<ICompetenciaService>();
            var competenciasResult = competenciaService.GetAll();
            comboBoxCompetencia.DataSource = competenciasResult.ListData;

            this.comboBoxCompetencia.DisplayMember = "Description";
            this.comboBoxCompetencia.ValueMember = "Id";

            this.comboBoxCompetencia.SelectedItem = _Current;
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Actualizar":
                    OnActualizar();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }
        void OnActualizar()
        {
            this._Current = (Competencia)this.comboBoxCompetencia.SelectedItem;
            this._grid.RefreshDataSource();
            this.Hide();
        }

    }
}