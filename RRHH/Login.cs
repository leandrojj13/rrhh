﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using DevExpress.XtraBars.Docking2010;
using RRHH.BL.Services;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Navigation;

namespace RRHH
{
    public partial class Login : DevExpress.XtraEditors.XtraForm
    {
        private List<BarButtonItem> listBarButtons;
        private List<AccordionControlElement> listAcordion;

        public Login(List<BarButtonItem> listBarButtons, List<AccordionControlElement> listAcordion)
        {
            InitializeComponent();
            this.listBarButtons = listBarButtons;
            this.listAcordion = listAcordion;
        }

        private void windowsUIButtonPanelMain_Click(object sender, ButtonEventArgs e)
        {
            ActionManager(e.Button.Properties.Caption);
        }

        void OnEnter() {
            var userService = Program.GetIntance<IUserService>();

            var resultSave = userService.VerifyUser(nombreUsuarioTxt.Text, passwordTxt.Text);

            if (resultSave.Success)
            {
                var user = resultSave.Data;

                if (user != null)
                {
                    CurrentInfo.CurrentUser = user;
                    ManageElementWhenIsLogged();
                }
                else {
                    MessageBox.Show("Favor verificar las credenciales nuevamente!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                this.Hide();
            }
            else
            {
                MessageBox.Show("Error de ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ActionManager(string caption)
        {
            switch (caption)
            {
                case "Ingresar":
                    OnEnter();
                    break;
                case "Cancelar":
                    this.Hide();
                    break;
            }
        }

        void ManageElementWhenIsLogged()
        {
            foreach (var item in listAcordion)
            {
                item.Visible = true;

                if (item.Text == "Inicio") {
                    item.Visible = false;
                }
            }

            foreach (var item in listBarButtons)
            {
                item.Visibility = BarItemVisibility.Always;

                if (item.Caption == "Inicio" ||  item.Caption.StartsWith("Iniciar Sesi")) {
                    item.Visibility = BarItemVisibility.Never;
                }

            }
        }

    }
}