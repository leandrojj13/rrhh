﻿namespace RRHH.Bl.Extensions
{
    public class SelectItemOption
    {
        public string Key { get; set; }
        public int Value { get; set; }

    }
}