﻿using RRHH.BL.Repository;
using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Util
{
    public interface IBaseService<T> {
        Result<T> GetAll();
        Result<T> Save(T entity);
        Result<T> Update(T entity);
        Result<T> Delete(T entity);
        Result<T> DeleteById(int Id);
    }

    public class BaseService<T> where T : class, IBaseEntity, new()
    {
        public IBaseRepository<T> _repository { get; set; }

        public BaseService(IBaseRepository<T> repository)
        {
            this._repository = repository;
        }

        public virtual Result<T> GetAll()
        {

            var result = new Result<T>();

            try
            {
                result.ListData = _repository.GetAll().ToList();
                result.Success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

                return result;
            }
        }
        public Result<T> Save(T entity)
        {

            var result = new Result<T>();

            try
            {
                this._repository.Add(entity);
                this._repository.SaveChanges();

                result.Success = true;
                result.Data = entity;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
                result.Data = entity;

                return result;
            }
        }
        public Result<T> Update(T entity)
        {

            var result = new Result<T>();

            try
            {
                this._repository.Update(entity);
                this._repository.SaveChanges();

                result.Success = true;
                result.Data = entity;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

                return result;
            }
        }
        public Result<T> Delete(T entity)
        {

            var result = new Result<T>();

            try
            {
                this._repository.Delete(entity);
                this._repository.SaveChanges();

                result.Data = entity;
                result.Success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

                return result;
            }
        }
        public Result<T> DeleteById(int Id)
        {

            var result = new Result<T>();
            var entityDeleted = _repository.FindById(Id);
            try
            {
                this._repository.Delete(entityDeleted);
                this._repository.SaveChanges();

                result.Data = entityDeleted;
                result.Success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

                return result;
            }
        }
       
    }
}
