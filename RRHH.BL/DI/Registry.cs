﻿using RRHH.BL.Repository;
using RRHH.BL.Services;
using RRHH.Model.Entities;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.DI
{
    public class Registry
    {
        static Container _container;


        public static Container GetContainer() {

            _container = new Container();

            _container.Register<IUserService, UserService>();
            _container.Register<IEmpleadoService, EmpleadoService>();
            _container.Register<ICandidatoService, CandidatoService>();
            _container.Register<IEnumService, EnumService>();
            _container.Register<IPuestoService, PuestoService>();
            _container.Register<ICapacitacionService, CapacitacionService>();
            _container.Register<ICompetenciaService, CompetenciaService>();
            _container.Register<IIdiomaService, IdiomaService>();
            _container.Register(typeof(IBaseRepository<>), typeof(BaseRepository<>));

            return _container;
        }


    }
}
