﻿using RRHH.Model.Context;
using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class, IBaseEntity, new()
    {
        protected RRHHContext _context;
        public DbSet<T> _set;
        public BaseRepository()
        {
            _context = new RRHHContext();
            _set = _context.Set<T>();
        }
        public void Add(T Entity)
        {
            _set.Add(Entity);
        }

        public void Delete(T Entity)
        {
            Entity.Deleted = true;
            Update(Entity);
        }

        public T FindById(int id)
        {
            var entity = _set.FirstOrDefault(x => x.Id == id && !x.Deleted);
            return entity;
        }

        public IEnumerable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return _set.Where(predicate).ToList();
        }

        public IEnumerable<T> GetAll()
        {
            return _set.Where(x=> !x.Deleted).ToList();
        }


        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            return _set.FirstOrDefault(predicate);
        }

        public IEnumerable<T> GetAll(params Expression<Func<T, object>>[] include)
        {
            var query = _set.AsQueryable();

            foreach (var item in include)
            {
                query = query.Include(item);
            }

            return query.ToList();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Update(T Entity)
        {
            _context.Entry(Entity).State = EntityState.Modified;
        }


    }
}
