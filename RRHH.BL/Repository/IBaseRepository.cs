﻿using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace RRHH.BL.Repository
{
    public interface IBaseRepository<T> where T : class, IBaseEntity, new()
    {
        void Add(T Entity);
        void Delete(T Entity);
        T FindById(int id);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(params Expression<Func<T, object>>[] include);
        IEnumerable<T> Where(Expression<Func<T, bool>> predicate);
        T FirstOrDefault(Expression<Func<T, bool>> predicate);
        void SaveChanges();
        void Update(T Entity);
    }
}