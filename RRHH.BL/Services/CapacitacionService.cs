﻿using RRHH.BL.Repository;
using RRHH.BL.Util;
using RRHH.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Services
{

    public interface ICapacitacionService : IBaseService<Capacitacion>
    {
    }

    public class CapacitacionService : BaseService<Capacitacion>, ICapacitacionService
    {
        public CapacitacionService(IBaseRepository<Capacitacion> repository) : base(repository)
        {
        }

    }
}
