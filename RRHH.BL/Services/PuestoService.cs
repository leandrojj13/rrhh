﻿using RRHH.BL.Repository;
using RRHH.BL.Util;
using RRHH.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Services
{
    public interface IPuestoService : IBaseService<Puesto>
    {
    }

    public class PuestoService : BaseService<Puesto>, IPuestoService
    {
        public PuestoService(IBaseRepository<Puesto> repository) : base(repository)
        {
        }

    }
}
