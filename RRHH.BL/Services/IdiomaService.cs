﻿using RRHH.BL.Repository;
using RRHH.BL.Util;
using RRHH.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Services
{
    public interface IIdiomaService : IBaseService<Idioma>
    {
    }

    public class IdiomaService : BaseService<Idioma>, IIdiomaService
    {
        public IdiomaService(IBaseRepository<Idioma> repository) : base(repository)
        {
        }
        
    }
}
