﻿using RRHH.BL.Repository;
using RRHH.BL.Util;
using RRHH.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Services
{

    public interface IUserService : IBaseService<User>
    {
        Result<User> VerifyUser(string username, string password);
    }

    public class UserService : BaseService<User>, IUserService
    {

        public UserService(IBaseRepository<User> repository) : base(repository)
        {
        }


        public  Result<User> VerifyUser(string username, string password)
        {
            var result = new Result<User>();

            try
            {
                var user =  _repository.FirstOrDefault(x => x.UserName.ToLower().Equals(username) && x.Password.Equals(password));
                result.Data = user;
                result.Success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

                return result;
            }
        }



    }
}
