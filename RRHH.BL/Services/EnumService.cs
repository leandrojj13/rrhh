﻿using RRHH.Bl.Extensions;
using RRHH.BL.Extensions;
using RRHH.BL.Util;
using RRHH.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Services
{
    public interface IEnumService
    {
        Result<SelectItemOption> GetAllNivelCapacitacion();
        Result<SelectItemOption> GetAllNivelRiesgoPuesto();
    }


    public class EnumService : IEnumService
    {
        public Result<SelectItemOption> GetAllNivelCapacitacion()
        {
            var result = new Result<SelectItemOption>();

            try
            {
                result.ListData = new NivelCapacitacion().ToListOptions().ToList();
                result.Success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

                return result;
            }
        }
        public Result<SelectItemOption> GetAllNivelRiesgoPuesto()
        {
            var result = new Result<SelectItemOption>();

            try
            {
                result.ListData = new NivelRiesgoPuesto().ToListOptions().ToList();
                result.Success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

                return result;
            }
        }

    }
}
