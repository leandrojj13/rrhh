﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Services
{
    public class HelloService : IHelloService
    {
        public string GetHelloWorld()
        {
            return "Hello World";
        }
    }

    public interface IHelloService
    {
        string GetHelloWorld();
    }
}
