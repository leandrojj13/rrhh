﻿using RRHH.BL.Repository;
using RRHH.BL.Util;
using RRHH.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Services
{
    public interface ICandidatoService : IBaseService<Candidato>
    {
    }

    public class CandidatoService : BaseService<Candidato>, ICandidatoService
    {
        public CandidatoService(IBaseRepository<Candidato> repository) : base(repository)
        {
        }

        public override Result<Candidato> GetAll()
        {
            var result = new Result<Candidato>();

            try
            {
                result.ListData = _repository.GetAll(x=> x.Capacitaciones, x=> x.Competencias, x => x.ExperienciasLaboral).ToList();
                result.Success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

                return result;
            }

        }
    }
}
