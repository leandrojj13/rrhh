﻿using RRHH.BL.Repository;
using RRHH.BL.Util;
using RRHH.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Services
{
    public interface ICompetenciaService : IBaseService<Competencia>
    {
    }

    public class CompetenciaService : BaseService<Competencia>, ICompetenciaService
    {
        public CompetenciaService(IBaseRepository<Competencia> repository) : base(repository)
        {
        }

    }
}
