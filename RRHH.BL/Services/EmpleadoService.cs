﻿using RRHH.BL.Repository;
using RRHH.BL.Util;
using RRHH.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.BL.Services
{

    public interface IEmpleadoService : IBaseService<Empleado>
    {
    }

    public class EmpleadoService : BaseService<Empleado>, IEmpleadoService
    {
        public EmpleadoService(IBaseRepository<Empleado> repository) : base(repository)
        {
        }

        public override Result<Empleado> GetAll()
        {
            var result = new Result<Empleado>();

            try
            {
                result.ListData = _repository.GetAll(x=> x.Puesto).ToList();
                result.Success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;

                return result;
            }
        }
    }
}
