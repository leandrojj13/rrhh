namespace RRHH.Model.Migrations
{
    using RRHH.Model.Context;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RRHH.Model.Context.RRHHContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RRHH.Model.Context.RRHHContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            AddUser(context);
        }

        void AddUser(RRHHContext context)
        {

            var any = context.Usuarios.Any();

            if (!any) {
                context.Usuarios.Add(new Entities.User
                {
                    UserName = "Admin",
                    Password = "RRHH1234,"
                });

                context.SaveChanges();
            }
        }
    }
}
