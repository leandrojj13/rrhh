namespace RRHH.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Candidatos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cedula = c.String(nullable: false),
                        Nombre = c.String(nullable: false),
                        PuestoId = c.Int(nullable: false),
                        Departamento = c.String(nullable: false),
                        SalarioAspira = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RecomendadoPor = c.String(),
                        Deleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ModifiedDate = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Puestos", t => t.PuestoId, cascadeDelete: true)
                .Index(t => t.PuestoId);
            
            CreateTable(
                "dbo.Capacitaciones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        FechaDesde = c.DateTime(nullable: false),
                        FechaHasta = c.DateTime(nullable: false),
                        Nivel = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ModifiedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        Candidato_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Candidatos", t => t.Candidato_Id)
                .Index(t => t.Candidato_Id);
            
            CreateTable(
                "dbo.Competencias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ModifiedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        Candidato_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Candidatos", t => t.Candidato_Id)
                .Index(t => t.Candidato_Id);
            
            CreateTable(
                "dbo.ExperienciasLaboral",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Empresa = c.String(),
                        PuestoOcupado = c.String(),
                        FechaDesde = c.DateTime(nullable: false),
                        FechaHasta = c.DateTime(nullable: false),
                        Salario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Deleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ModifiedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        Candidato_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Candidatos", t => t.Candidato_Id)
                .Index(t => t.Candidato_Id);
            
            CreateTable(
                "dbo.Puestos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        NivelRiesgo = c.Int(nullable: false),
                        MinimoSalario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaximoSalario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Deleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ModifiedDate = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Empleados",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cedula = c.String(nullable: false),
                        Nombre = c.String(nullable: false),
                        FechaIngreso = c.DateTime(nullable: false),
                        FechaSalida = c.DateTime(),
                        PuestoId = c.Int(nullable: false),
                        SalarioMensual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Deleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ModifiedDate = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Puestos", t => t.PuestoId, cascadeDelete: true)
                .Index(t => t.PuestoId);
            
            CreateTable(
                "dbo.Idiomas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ModifiedDate = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(),
                        Deleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        ModifiedBy = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ModifiedDate = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Empleados", "PuestoId", "dbo.Puestos");
            DropForeignKey("dbo.Candidatos", "PuestoId", "dbo.Puestos");
            DropForeignKey("dbo.ExperienciasLaboral", "Candidato_Id", "dbo.Candidatos");
            DropForeignKey("dbo.Competencias", "Candidato_Id", "dbo.Candidatos");
            DropForeignKey("dbo.Capacitaciones", "Candidato_Id", "dbo.Candidatos");
            DropIndex("dbo.Empleados", new[] { "PuestoId" });
            DropIndex("dbo.ExperienciasLaboral", new[] { "Candidato_Id" });
            DropIndex("dbo.Competencias", new[] { "Candidato_Id" });
            DropIndex("dbo.Capacitaciones", new[] { "Candidato_Id" });
            DropIndex("dbo.Candidatos", new[] { "PuestoId" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.Idiomas");
            DropTable("dbo.Empleados");
            DropTable("dbo.Puestos");
            DropTable("dbo.ExperienciasLaboral");
            DropTable("dbo.Competencias");
            DropTable("dbo.Capacitaciones");
            DropTable("dbo.Candidatos");
        }
    }
}
