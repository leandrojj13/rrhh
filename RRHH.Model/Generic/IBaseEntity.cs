﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Generic
{
    public interface IBaseEntity
    {
        int Id { get; set; }
        bool Deleted { get; set; }
    }

    public class BaseEntity : AuditableEntity, IBaseEntity
    {
        public int Id { get; set; }
        public bool Deleted { get; set; }
    }
}
