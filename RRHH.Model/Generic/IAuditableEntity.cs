﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Generic
{
    public  interface IAuditableEntity
    {
        int CreatedBy { get; set; }
        int ModifiedBy { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset ModifiedDate { get; set; }
    }

    public class AuditableEntity : IAuditableEntity
    {
        public int CreatedBy { get ; set; }
        public int ModifiedBy { get ; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset ModifiedDate { get; set; }
    }
}
