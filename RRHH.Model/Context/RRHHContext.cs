﻿using RRHH.Model.Entities;
using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Context
{
    public class RRHHContext : DbContext
    {
        public RRHHContext() : base("RRHHConnectionString")
        {
        }

        public override int SaveChanges()
        {
            RunAudit();
            return base.SaveChanges();
        }
        private void RunAudit() 
        {
            CreateAuditLogInformation();
        }

        public void CreateAuditLogInformation()
        {
            foreach (var entry in ChangeTracker.Entries<IAuditableEntity>().Where(x => x.State == EntityState.Added))
            {
                var entity = entry.Entity;
                    entity.CreatedDate = DateTime.Now;
            }

            foreach (var entry in ChangeTracker.Entries<IAuditableEntity>().Where(x => x.State == EntityState.Modified))
            {
                var entity = entry.Entity;
                entity.ModifiedDate = DateTime.Now;
            }
        }

        public DbSet<User> Usuarios { get; set; }
        public DbSet<Candidato> Candidatos { get; set; }
        public DbSet<Capacitacion> Capacitaciones { get; set; }
        public DbSet<Competencia> Competencias { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<ExperienciaLaboral> ExperienciasLaboral { get; set; }
        public DbSet<Idioma> Idiomas { get; set; }
        public DbSet<Puesto> Puestos { get; set; }
    }
}
