﻿using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Entities
{
    [Table("Idiomas")]
    public class Idioma : BaseEntity
    {
        [Required]
        public string Nombre { get; set; }
    }
}
