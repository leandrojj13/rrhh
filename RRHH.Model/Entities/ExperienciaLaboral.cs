﻿using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Entities
{
    [Table("ExperienciasLaboral")]
    public class ExperienciaLaboral : BaseEntity
    {
        public string Empresa { get; set; }
        public string PuestoOcupado { get; set; }
        public DateTime  FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public decimal Salario { get; set; }
    }
}
