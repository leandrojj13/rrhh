﻿using RRHH.Model.Enums;
using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Entities
{
    [Table("Puestos")]
    public class Puesto : BaseEntity
    {
        [Required]
        public string Nombre { get; set; }
        [Required]
        public NivelRiesgoPuesto NivelRiesgo { get; set; }
        [Required]
        public decimal MinimoSalario { get; set; }
        [Required]
        public decimal MaximoSalario { get; set; }
    }
}
