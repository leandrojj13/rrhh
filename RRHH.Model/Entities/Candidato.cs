﻿using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Entities
{
    [Table("Candidatos")]
    public class Candidato : BaseEntity
    {
        [Required]
        public string Cedula { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public int PuestoId { get; set; }
        [Required]
        public string Departamento { get; set; }
        [Required]
        public decimal SalarioAspira { get; set; }
        public string RecomendadoPor { get; set; }
        [NotMapped]
        public string PuestoNombre { get {   return Puesto.Nombre;   } }

        public virtual Puesto Puesto { get; set; }
        public List<Competencia> Competencias { get; set; }
        public List<Capacitacion> Capacitaciones { get; set; }
        public List<ExperienciaLaboral> ExperienciasLaboral { get; set; }
    }
}
