﻿using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Entities
{
    [Table("Empleados")]
    public class Empleado : BaseEntity
    {
        [Required]
        public string Cedula { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public DateTime FechaIngreso { get; set; }
        public DateTime? FechaSalida { get; set; }
        [Required]
        public int PuestoId { get; set; }
        [Required]
        public decimal SalarioMensual { get; set; }

        [NotMapped]
        public string PuestoNombre { get { return Puesto.Nombre; } }

        public Puesto Puesto { get; set; }
    }
}
