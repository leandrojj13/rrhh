﻿using RRHH.Model.Enums;
using RRHH.Model.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Entities
{
    [Table("Capacitaciones")]
    public class Capacitacion : BaseEntity
    {
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        [Required]
        public NivelCapacitacion Nivel { get; set; }
    }
}
