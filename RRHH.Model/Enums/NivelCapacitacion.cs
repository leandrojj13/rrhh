﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Enums
{
    public enum NivelCapacitacion
    {
        [Description("Grado")]
        Grado,
        [Description("Post-Grado")]
        PostGrado,
        [Description("Maestría Doctorado")]
        MaestriaDoctorado,
        [Description("Técnico")]
        Tecnico,
        [Description("Gestión")]
        Gestion
    }
}
