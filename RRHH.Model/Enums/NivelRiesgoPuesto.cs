﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRHH.Model.Enums
{
    public enum NivelRiesgoPuesto
    {
        [Description("Alto")]
        Alto,
        [Description("Medio")]
        Medio,
        [Description("Bajo")]
        Bajo
    }
}
